from django.contrib import admin
from projects.models import Project
from tasks.models import Task


@admin.register(Project)
class Project(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
        "id",
    )


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
        "id",
    )
